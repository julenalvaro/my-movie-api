from config.database import Base
from sqlalchemy import Column, Integer, String, Float

#Creamos la clase Movie que hereda de Base, por lo que es una clase que representa una tabla en la BD
class Movie(Base):
    __tablename__ = 'movies1'
    id = Column(Integer, primary_key=True)
    title = Column(String(255))
    overview = Column(String(255))
    year = Column(Integer)
    category = Column(String(255))
    rating = Column(Float)