import os
from sqlalchemy import create_engine
from sqlalchemy.orm.session import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

sqlite_file_name = '../database.sqlite'

#leer el directorio actual de este propio archivo
base_dir = os.path.dirname(os.path.realpath(__file__)) 
#Construir ruta completa de la BD
database_url = f'sqlite:///{os.path.join(base_dir, sqlite_file_name)}'

engine = create_engine(database_url, echo=True)

#Creamos una sesión para conectarnos a la BD
Session = sessionmaker(bind=engine)

Base = declarative_base()